var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');

// New Code
var monk = require('monk');
var db = monk('localhost:27017/firstrest');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
let apiRoutes = require("./routes/routes")

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());



app.post("/someRoute", function(req, res) {
    console.log("req.body:"+req.body.name);
    res.send({ status: 'SUCCESS' });
});

const request = require("supertest");

app.get("/usersAndGroups", async function(req, res) {
    const client = request(req.app);
    const users = await client.get("/users");
    const groups = await client.get("/groups");

    res.json({
        users: users.body,
        groups: groups.body
    });
});

//connect to mongoose
const dbPath = 'mongodb://localhost/firstrest';
const options = {useNewUrlParser: true, useUnifiedTopology: true}
const mongo = mongoose.connect(dbPath, options);
mongo.then(() => {
    console.log('connected fine');
}, error => {
    console.log(error, 'error');
})


// Make our db accessible to our router
app.use(function(req,res,next){
 req.db = db;
 next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRoutes)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
 next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
// set locals, only providing error in development
res.locals.message = err.message;
res.locals.error = req.app.get('env') === 'development' ? err : {};

// render the error page
res.status(err.status || 500);
res.render('error');
});

module.exports = app;