Steps:

1) Need to install node software and check the version as below.
node -v

2) Need to install  npm and check the Npm Version as below.
npm -v

3) Need to install  mongo db and check the Npm Version as below.
mongo --version

Start the mongo db
brew services start mongodb-community@4.4

4) Create an account at the below site and get below curl command working.

curl -H "X-CMC_PRO_API_KEY: ec6d3d55-a3ac-490d-8fb2-d78032cc2593" -H "Accept: application/json" -d "start=1&limit=5000&convert=USD" -G https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest

5) Checkout the project from below bitbuket url.


6) Go to the directory FirstRest and run below command to get necessary libraries installed.

	npm install

7) Start the npm node server with below command.

	npm start


8) Import the Postman collections and run the register request.

Post - http://localhost:3000/api/bio/register

9) Run the login request.

Post - http://localhost:3000/api/bio/login


10) Run the listcrypto request by passing x-access-token header value as token from the registered user.

Post - http://localhost:3000/api/bio/listcrypto

11) Setup the MongoDB Compass.

View the data in the document db.





