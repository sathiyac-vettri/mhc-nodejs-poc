
var express = require('express');
var router = express.Router();
var request = require('request');
var http = require('http');
var https = require('https');
var fs = require("fs");


/* Ignore this */
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


module.exports = router;
