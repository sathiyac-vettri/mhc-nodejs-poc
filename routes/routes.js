
//routes.js
//initialize express router
let router = require('express').Router();
let fetch = require('node-fetch');
const jwt = require('jsonwebtoken');

//set default API response
router.get('/', function(req, res) {
    res.json({
        status: 'API Works',
        message: 'Welcome to FirstRest API'
    });
});
//Import Bio Controller
var bioController = require('./bioController');
// Bio routes
router.route('/bio/register')
    .post(bioController.register);
router.route('/bio/login')
    .post(bioController.login);
router.route('/bio/allusers')
    .get(bioController.allusers);
router.route('/bio/:bio_id')
    .get(bioController.view)
    .patch(bioController.update)
    .put(bioController.update)
    .delete(bioController.delete);
router.route('/bio/createtoken')
    .post(bioController.createtoken);
router.route('/bio/verifytoken')
    .post(bioController.verifytoken);
router.route('/bio/listcrypto')
    .post(bioController.listcrypto);


function handleErrors(response) {
    if(!response.ok) {
    throw new Error("Request failed " + response.statusText);
    }
    return response;
}

//Export API routes
module.exports = router;


