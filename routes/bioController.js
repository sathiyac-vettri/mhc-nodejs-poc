

//bioController.js

'use strict';
const util = require('util'),
//Import Bio Model
Bio = require('./bioModel');

var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var request = require('request');
var https = require('https');
var http = require('http');

//For index
exports.allusers = function (req, res) {
    Bio.get(function (err, bio) {
        if (err)
            res.json({
                status: "error",
                message: err
            });
        res.json({
            status: "success",
            message: "Got Bio Successfully!",
            data: bio       
        });
    });
};

//For creating new bio
exports.register = function (req, res) {
    var bio = new Bio();
    var obj = req.body;

    console.log('sathiya:'+util.inspect(obj, {depth: null}));
    console.log('req:'+req);  
    bio.name = req.body.name? req.body.name: bio.name;
    bio.email = req.body.email;
    bio.phone = req.body.phone;
    bio.address = req.body.address;
    bio.password = bcrypt.hashSync(req.body.password, 8);
    
    const token = jwt.sign(
      { userId: bio._id },
      //'RANDOM_TOKEN_SECRET',
      config.secret,
      { expiresIn: '24h' });

bio.token = token; 
//Save and check error
    bio.save(function (err) {
        if (err)
            res.json(err);
    res.json({
            message: "New Bio Added YES!",
            data: bio
        });
    });

};


exports.login = (req, res, next) => {

  Bio.findOne({ 'email' : req.body.email })
  .then(
    (user) => {
      if (!user) {
        return res.status(401).json({
          error: new Error('User not found!'),
          msg: 'User not found!'
        });
      }else{

        if(!bcrypt.compareSync(req.body.password, user.password)) {
          return res.status(401).json({
            error: new Error('Authenticated Failed, Invalid user'),
            msg: 'Authenticated Failed, Invalid user'
          });
        } 

        return res.status(200).json({
          user: user
        });
      }
      console.log("sathiya user:"+user);
    }
  ).catch(
    (error) => {
      res.status(500).json({
        error: error
      });
    }
  );
};

// View Bio
exports.view = function (req, res) {
    Bio.findById(req.params.bio_id, function (err, bio) {
        if (err)
            res.send(err);
        res.json({
            message: 'Bio Details',
            data: bio
        });
    });
};

// Update Bio
exports.update = function (req, res) {
    Bio.findById(req.params.bio_id, function (err, bio) {
        if (err)
            res.send(err);
        bio.name = req.body.name ? req.body.name : bio.name;
        bio.email = req.body.email;
        bio.phone = req.body.phone;
        bio.address = req.body.address;
        bio.password = bcrypt.hashSync(req.body.password, 8);
//save and check errors
        bio.save(function (err) {
            if (err)
                res.json(err)
            res.json({
                message: "Bio Updated Successfully",
                data: bio
            });
        });
    });
};

// Delete Bio
exports.delete = function (req, res) {
    Bio.deleteOne({
        _id: req.params.bio_id
    }, function (err, contact) {
        if (err)
            res.send(err)
        res.json({
            status: "success",
            message: 'Bio Deleted'
        })
    })
};

//createtoken
exports.createtoken = (req, res, next) => {
    Bio.findOne({ email: req.body.email }).then(
      (user) => {
        if (!user) {
          return res.status(401).json({
            error: new Error('User not found!')
          });
        }
        console.log("sathiya:"+user);
        bcrypt.compare(req.body.password, user.password).then(
          (valid) => {
            if (!valid) {
              return res.status(401).json({
                error: new Error('Incorrect password!')
              });
            }
            console.log("sathiya valid:"+valid);
            const token = jwt.sign(
              { userId: user._id },
              'RANDOM_TOKEN_SECRET',
              { expiresIn: '24h' });

              console.log("sathiya token:"+token);
            res.status(200).json({
              userId: user._id,
              token: token
            });

            if (user) {
              user.token = token;
              //save and check errors
              user.save(function (err) {
                      if (err)
                          res.json(err)
                      res.json({
                          message: "Bio Updated Successfully",
                          data: bio
                      });
                  });
            }

          }
        ).catch(
          (error) => {
            res.status(500).json({
              error: error
            });
          }
        );

  

      }
    ).catch(
      (error) => {
        res.status(500).json({
          error: error
        });
      }
    );
  };

  //verifytoken
  exports.verifytoken = (req, res, next) => {
    var headertoken = req.headers['x-access-token'];
    Bio.findOne({ token: headertoken }).then(
      (user) => {
        if (!user) {
          return res.status(401).json({
            error: new Error('User not found!')
          });
        }else{
          return res.status(200).json({
            user: user
          });
        }
        console.log("sathiya:"+user);

      }
    ).catch(
      (error) => {
        res.status(500).json({
          error: error
        });
      }
    );
  }

  //listcrypto  
  exports.listcrypto = (req, res, next) => {
    var headertoken = req.headers['x-access-token'];
    console.log("headertoken:"+headertoken);

    Bio.findOne({ 'token': headertoken }).then(
      (user) => {
        if (!user) {
          return res.status(401).json({
            error: new Error('Header token is invalid'),
            msg: 'Header token is invalid'
          });
        }else{
         
            console.log("else called:");
            const postData = JSON.stringify({
              'start':'1',
              'limit':'1',
              'convert':'USD'
            });

            const rp = require('request-promise');
            const requestOptions = {
              method: 'GET',
              uri: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
              qs: {
                'start': '1',
                'limit': '1',
                'convert': 'USD'
              },
              headers: {
                'X-CMC_PRO_API_KEY': 'ec6d3d55-a3ac-490d-8fb2-d78032cc2593'
              },
              json: true,
              gzip: true
            };

            rp(requestOptions).then(response => {
              console.log('API call response:', response);
              return res.status(200).json({
                        msg: 'success',
                        data: response
                      });
            }).catch((err) => {
              console.log('API call error:', err.message);
            });
        }
        console.log("sathiya user:"+user);
        
      }
    ).catch(
      (error) => {
        res.status(500).json({
          error: error
        });
      }
    );
  }

  